import re


class AbstractOptimiser:
    def process(self, graph):
        graph = self.pre_process(graph)

        result = self.process_internal(graph) if len(graph) >= 3 else graph

        return self.post_process(result)

    def pre_process(self, graph):
        return graph

    def process_internal(self, graph):
        pass

    def post_process(self, result):
        return result

    def _is_num(self, num):
        res = re.fullmatch(
            r'-?(((0{1})|([1-9]\d*)).?\d*)(e?(\+|-)?[1-9]\d*)?', num)
        return res is not None

    def _to_num(self, num):
        if int(num) == float(num):
            return int(num)
        return float(num)


class DoubleNegativeOptimiser(AbstractOptimiser):
    def process_internal(self, graph):
        length = len(graph)
        for _ in range(length):
            found = False
            for i in range(1, len(graph)):
                if graph[i-1] == '$' and graph[i] == '$':
                    graph[i-1], graph[i] = '', ''
                    found = True
            graph = ' '.join(graph).split()
            if not found:
                break

        for i in range(1, len(graph)):
            if graph[i-1] == '$' and graph[i] == '-':
                graph[i-1], graph[i] = '', '+'

        graph = ' '.join(graph).split()

        return(graph)


class IntegerCostantsOptimiser(AbstractOptimiser):
    _oper_dict = {
        '+': lambda x, y: x + y,
        '-': lambda x, y: x - y,
        '*': lambda x, y: x * y,
        '/': lambda x, y: x // y,
        '^': lambda x, y: x ** y,
    }

    def _negative_numbers(self, graph):
        for i in range(1, len(graph)):
            if self._is_num(graph[i-1]) and graph[i] == '$':
                graph[i-1] = str(-self._to_num(graph[i-1]))
                graph[i] = ''
        graph = ' '.join(graph).split()

    def _operations_on_const(self, graph):
        if len(graph) < 3:
            return
        for i in range(2, len(graph)):
            if (
                self._is_num(graph[i-2])
                and self._is_num(graph[i-1])
                and graph[i] in '+-*/^'
            ):
                graph[i] = self._oper_dict[graph[i]](
                    self._to_num(graph[i-2]), self._to_num(graph[i-1]))
                graph[i] = str(self._to_num(str(graph[i])))
                graph[i-2], graph[i-1] = '', ''
        graph = ' '.join(graph).split()

    def _back_to_rpn(self, graph):
        for i in range(len(graph)):
            if self._is_num(graph[i]) and self._to_num(graph[i]) < 0:
                graph[i] = graph[i][1:]
                graph.insert(i+1, '$')
        graph = ' '.join(graph).split()

    def process_internal(self, graph):
        self._negative_numbers(graph)
        self._operations_on_const(graph)
        self._back_to_rpn(graph)
        return graph


class SimplifierOptimiser(AbstractOptimiser):
    def process_internal(self, graph):
        oper_count = 0
        for index in range(len(graph)):
            if graph[index] in '+-*/^':
                oper_count += 1
        for _ in range(oper_count):
            graph = self._add_zero_mul_one(graph)
            graph = self._eq_sub_div(graph)
            graph = self._mul_zero(graph)
            graph = self._exp_to_zero(graph)
        return graph

    def _climber(self, graph, index):
        if index <= 0:
            return 0
        if graph[index] not in '+-*/^':
            return index
        ind = self._climber(graph, index-1)
        return self._climber(graph, ind - 1)

    def _add_zero_mul_one(self, graph):
        if len(graph) < 3:
            return graph
        for i in range(2, len(graph)):
            if (graph[i-1] == '0' and graph[i] in '+-') or (
                graph[i-1] == '1' and graph[i] in '*/'
            ):
                graph[i-1], graph[i] = '', ''
        graph = ' '.join(graph).split()
        if len(graph) > 2:
            for i in range(2, len(graph)):
                if (
                    graph[i] == '+' and graph[self._climber(graph, i)] == '0'
                ) or (
                    graph[i] == '*' and graph[self._climber(graph, i)] == '1'
                ):
                    graph[i] = ''
                    graph[self._climber(graph, i)] = ''
        graph = ' '.join(graph).split()
        return graph

    def _mul_zero(self, graph):
        if len(graph) < 3:
            return graph
        for i in range(2, len(graph)):
            if graph[i] == '*' and (
                graph[i-1] == 0 or graph[self._climber(graph, i) == '0']
            ):
                for j in range(self._climber(graph, i), i+1):
                    graph[j] = ''
        graph = ' '.join(graph).split() or ['0']
        return graph

    def _eq_sub_div(self, graph):
        if len(graph) < 3:
            return graph
        for i in range(2, len(graph)):
            if graph[i] in '-/':
                left_ind = self._climber(graph, i)
                right_ind = self._climber(graph, i-1)
                if graph[left_ind:right_ind] == graph[right_ind:i]:
                    k = '1' if graph[i] == '/' else '0'
                    graph[left_ind] = k
                    for j in range(left_ind+1, i+1):
                        graph[j] = ''
        graph = ' '.join(graph).split()
        return graph

    def _exp_to_zero(self, graph):
        if len(graph) < 3:
            return graph
        for i in range(2, len(graph)):
            if graph[i] == '^' and graph[i-1] == '0':
                left_ind = self._climber(graph, i)
                graph[left_ind] = '1'
                for j in range(left_ind+1, i+1):
                    graph[j] = ''
        graph = ' '.join(graph).split()
        return graph
