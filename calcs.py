import re
from optimisers import DoubleNegativeOptimiser
from optimisers import IntegerCostantsOptimiser
from optimisers import SimplifierOptimiser


class Calculator:
    def __init__(self, opcodes: list, operators=None):
        self.opcodes = opcodes
        self._prefix_minus()
        self.operators = operators if operators is not None else []
        self._rpn = self._to_rpn()

    def __str__(self) -> str:
        return ''.join(self._rpn).replace('$', '-')

    def _prefix_minus(self):
        if self.opcodes[0] == '-':
            self.opcodes[0] = '$'
        for index in range(1, len(self.opcodes)):
            if self.opcodes[index] == '-' and self.opcodes[index-1] == '(':
                self.opcodes[index] = '$'

    def _to_rpn(self):
        stack, result = [], []
        for code in self.opcodes:
            if code in '$(':
                stack.append(code)
            elif code == ')':
                oper = stack.pop()
                while oper != '(':
                    result.append(oper)
                    oper = stack.pop()
                if len(stack) > 0 and stack[-1] == '$':
                    result.append(stack.pop())
            elif code in '^':
                stack.append(code)
            elif code in '*/':
                if len(stack) > 0 and stack[-1] in '^/':
                    result.append(stack.pop())
                stack.append(code)
            elif code in '+-':
                if len(stack) > 0 and stack[-1] in '^/*-+':
                    result.append(stack.pop())
                stack.append(code)
            elif code.isalpha or self._is_num(code):
                result.append(code)
                if len(stack) > 0 and stack[-1] == '$':
                    result.append(stack.pop())
            else:
                return
        while len(stack) > 0:
            if stack[-1] not in '+-/*^$':
                return
            result.append(stack.pop())

        opers, obj = 0, 0
        for code in result:
            if code in '+-/*^':
                opers += 1
            else:
                if code != '$':
                    obj += 1

        return result if opers + 1 == obj else None

    def optimise(self):
        for operator in self.operators:
            self._rpn = operator.process(self._rpn)

    def _is_num(self, num):
        res = re.fullmatch(
            r'-?(((0{1})|([1-9]\d*)).?\d*)(e?(\+|-)?[1-9]\d*)?', num)
        return res is not None

    def _zero_division_check(self):
        return '/0' not in ''.join(self.opcodes)

    def _is_rpn(self):
        if len(self.opcodes) < 3:
            res = re.fullmatch(r'-?((\d+)|([a-zA-Z]+))', ''.join(self.opcodes))
            return res is not None
        return self._rpn is not None

    def validate(self) -> bool:
        return self._zero_division_check() and self._is_rpn()

    def calculate(self, var_values):
            # return result and number of operations
        pass


def test():
    validate_check_list = [
        ('a+2', True),
        ('a-(-2)', True),
        ('a+2-', False),
        ('a+(2+(3+5)', False),
        ('a^2', True),
        ('a^(-2)', True),
        ('-a-2', True),
        ('6/0', False),
        ('a/(b-b)', True),
    ]

    print('validate test fails:')
    for case, exp in validate_check_list:
        tokens = list(case)

        calc = Calculator(tokens).validate()
        if calc != exp:
            print(
                f'Error in case for "{case}". Actual "{calc}", expected {exp}')

    str_check_list = [
        ("a", "a"),
        ("-a", "a-"),
        ("(a*(b/c)+((d-f)/k))", "abc/*df-k/+"),
        ("(a)", "a"),
        ("a*(b+c)", "abc+*"),
        ("(a*(b/c)+((d-f)/k))*(h*(g-r))", "abc/*df-k/+hgr-**"),
        ("(x*y)/(j*z)+g", "xy*jz*/g+"),
        ("a-(b+c)", "abc+-"),
        ("a/(b+c)", "abc+/"),
        ("a^(b+c)", "abc+^"),
        ("-(-(-a))", "a---"),
    ]

    print('__str__ test fails:')
    for case, exp in str_check_list:
        tokens = list(case)
        calc = Calculator(tokens)

    if str(calc) != exp:
        print(f'Error in case for "{case}". Actual "{calc}", expected {exp}')

    double_negate_tests = [
        ('-(-a)', 'a'),
        ('-(-5)', '5'),
        ('-(a+b)+c+d', 'ab+-c+d+'),
        ('-(a+b)+c-(-d)', 'ab+-c+d+'),
    ]

    print('double_negative test fails:')
    for case, exp in double_negate_tests:
        tokens = list(case)
        calc = Calculator(tokens, [DoubleNegativeOptimiser()])
        calc.optimise()
        if str(calc) != exp:
            print(
                f'Error in case for "{case}". Actual "{calc}", expected {exp}')

    integer_constant_optimiser_tests = [
        (['1'], ['1']),
        (['1', '+', '2'], ['3']),
        (['1', '-', '2'], ['1-']),
        (['2', '*', '2'], ['4']),
        (['2', '/', '2'], ['1']),
        (['2', '^', '10'], ['1024']),
        (['a', '+', '2', '*', '4'], ['a8+', '8a+']),
        (['2', '+', 'a', '+', '3'], ['5a+', 'a5+']),    # (*)
    ]

    print('integer_constant_optimiser test fails:')
    for case, exp in integer_constant_optimiser_tests:
        calc = Calculator(
            case, [DoubleNegativeOptimiser(), IntegerCostantsOptimiser()])

        calc.optimise()

        if str(calc) not in exp:
            print(
                f'Error in case for "{case}". Actual "{calc}", expected one of {exp}')

    simplifier_optimiser_test = [
        ('a+0', ['a']),
        ('0+a', ['a']),
        ('a*1', ['a']),
        ('1*a', ['a']),
        ('a*0', ['0']),
        ('b/b', ['1']),
        ('a-a', ['0']),
        ('a+(b-b)', ['a']),
        ('a+(7-6-1)', ['a']),
        ('a^0', ['1']),
        ('a-(-(-a))', ['0']),
        ('a+a+a', ['a3*', '3a*']),  # (*)
        ('(a-b)-(a-b)', ['0']),  # (*)
        ('(a-b)/(a-b)', ['1']),  # (*)
        ('(a+b)+(a+b)', ['ab+2*', '2ab+*']),  # (*)
        ('a*b+a*b', ['2ab**', '2ba**', 'a2b**',
                     'ab2**', 'b2a**', 'ba2**']),  # (*)
    ]

    print('simplifier_optimiser fail tests:')
    for case, exps in simplifier_optimiser_test:
        tokens = list(case)
        calc = Calculator(tokens, [DoubleNegativeOptimiser(
        ), IntegerCostantsOptimiser(), SimplifierOptimiser()])

        calc.optimise()

        if str(calc) not in exps:
            print(
                f'Error in case for "{case}". Actual "{calc}", expected one of {exps}')


if __name__ == '__main__':
    test()
